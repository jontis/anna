## 2019-06-15
High level idea for system.
Extract tables from pdfs.
Persist table parts in file format? why? Sppedup? Caching?
    Use file name as identifier what is in the file.
Makes sure all tables that are in the labeled set are there.

One table file at a time, parse the table and check results.
Build accuracy measurements comparing with labeled set.
Build parser that takes configuration options.
Build supervisor layer that employs some decision about which order to try parsing configurations.
If lucky, one configuration per journal can do it.

### steps
* Create conform naming for extracted tables:
** 

## 2019-06-25
Have partial table identifier

Questions to solve:
* How to select tables? ML or rules? Use supplied csv as training?
* Try to autotune parameters for parser with ML? Using answer sheet as labeled set?

