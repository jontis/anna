#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 16:05:37 2019

component: identify and extract tables, verify with labeled data set
takes file with complete path as input argument

@author: jonathan
"""


import os
from commons.config import Config
#from commons.my_table import MyTable
from commons.file_properties import FileProperties

import roman
import PyPDF2
#import PyPDF4
import tabula
import camelot
import pandas as pd
import matplotlib.pyplot as plt
from checkpointer import checkpoint

#os.environ['CHECKPOINTS_VERBOSITY'] = '0'

def call_table_parser():
    pass


def get_cam_table_name(cam, verbose=False):
    # can you find 'table' in any cell in the top 2 rows?
    # returns table designation, table heading
    def check_reasonable_table_name(str_table):
        def check_reasonable_number(str_table):
            if str_table.isnumeric():
                if (0 < int(str_table)) and (int(str_table) < 20):
                    return True
                else:
                    return False
        
        # pure number
        if check_reasonable_number(str_table):
            return True
        # number with a letter in front
        if (str_table.lower()[0] in 'abcde') and check_reasonable_number(str_table[1:]):
            return True
        # number with a letter behind
        if (str_table.lower()[1:] in 'abcde') and check_reasonable_number(str_table[0]):
            return True        
        return False
    
    
    def try_roman(str_table):
        try:
            if check_reasonable_table_name(str(roman.fromRoman(str_table.upper()))):
                return True
        except:
            return False 


    for i_row in range(2):
        list_table_names = [i.split('table ')[1] for i in cam.df.iloc[i_row, :].apply(lambda x: str(x).lower()) if 'table' in i]
        if 0 < len(list_table_names):
            # is it a number?
            str_table = list_table_names[0]
            str_table = str_table.replace('.', '')
            if verbose: print(str_table)
            if type(str_table) is str:
                if check_reasonable_table_name(str_table):
                    return str_table, ''
                
                # try splitting on chars, remember that similar chars can be misinterpreted by the parser
                for split_char in ['-', '—', ',', ' ', '.']:               
                    if check_reasonable_table_name(str_table.split(split_char, 1)[0]):
                        return str_table.split(split_char, 1)            
                # try romans
                if try_roman(str_table):
                    return str(roman.fromRoman(str_table.upper())), ''
            if verbose: print('no table number found')
            return '', str_table
    if verbose: print('no table... string row found')
    return '', ''


def clean_common_misinterpreted_characters(str_in):
    {'fi': 'ﬁ'}
    return str_in


@checkpoint
def find_keywords_for_relevant_data_columns(df, keywords=['coefficient', 'statistic'], n_top_rows=5):
    # WIP 
    list_keywords =  ['coefficient', 'statistic', 't_stat', 'p_value', 'standard_deviation', 'variable']
    df_top = df.loc[:n_top_rows, :].copy()
    df_top.fillna('', inplace=True) # nans give improper detection
    
    list_found_keywords = []
    for i_key in list_keywords:
        for i_col in range(df_top.shape[1]):
            
            if sum(df_top.iloc[:, i_col].str.lower().str.find(i_key)!=-1): list_found_keywords.append(i_key)  
    
    return list_found_keywords

    
#    for i_row in range(n_top_rows): # search in top rows
#        list_table_names = [i.split('table ')[1] for i in cam.df.iloc[i_row, :].apply(lambda x: str(x).lower()) if 'table' in i]
#        if 0 < len(list_table_names):
#            print(list_table_names[0])


def get_first_page_number(str_text):
    n_first_page = str_text.split('[')[0] # let's hope it follows the same schematic or we need rules
    return n_first_page


@checkpoint
def get_pdf_n_pages(name_file_pdf):
    # how many pages?
    with open(name_file_pdf, 'rb') as pdfFileObj:
        # pdf reader object
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        n_pages = int(pdfReader.numPages)
    return n_pages


@checkpoint
def parse_pdf_article_first_page_wrapper(name_file_pdf, file_properties):
    # how many pages?
    if file_properties.journal == 'jpe':
        return parse_pdf_article_first_page_gen(name_file_pdf)
    if file_properties.journal == 'aer':
        return parse_pdf_article_first_page_gen(name_file_pdf)
    if file_properties.journal == 'qje':
        return parse_pdf_article_first_page_gen(name_file_pdf)
        
    # general casee:
    return parse_pdf_article_first_page_gen(name_file_pdf)        



@checkpoint
def parse_pdf_article_first_page_gen(name_file_pdf):
    def check_ok_page_numer(n_first_page):
        try:
            if int(n_first_page) < 10000:
                return True
            else:
                return False
        except:
            return False

        
    def get_first_page_number(pdfReader):
        # first aep method
        pageObj = pdfReader.getPage(0)
        str_text = pageObj.extractText()
        n_first_page = str_text.split('[')[0] # let's hope it follows the same schematic or we need rules
        if check_ok_page_numer(n_first_page): return n_first_page

        pageObj = pdfReader.getPage(1)
        str_text = pageObj.extractText()
        n_first_page = str_text.split('[')[0] # let's hope it follows the same schematic or we need rules
        if check_ok_page_numer(n_first_page): return n_first_page
        
        # all failed
        return 0
        
        
    with open(name_file_pdf, 'rb') as pdfFileObj:
        # pdf reader object
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        n_first_page = get_first_page_number(pdfReader)
    return n_first_page


@checkpoint
def checkpointed_tabula_read(name_file_pdf, i_page):
    return tabula.read_pdf(name_file_pdf, silent=True, multiple_tables=True, pages=i_page)


def wrapper_tabula(name_file_pdf, i_page):
    list_dict = []
    dict_parse = {'page_index': i_page, 'status': 'pass'}
    list_df = checkpointed_tabula_read(name_file_pdf, i_page)
    for j, j_df in enumerate(list_df):
        dict_parse['rows'] = j_df.shape[0]
        dict_parse['cols'] = j_df.shape[1]
        list_found_keywords = find_keywords_for_relevant_data_columns(j_df)
        dict_parse['repeated_keys'] = True if (len(set(list_found_keywords)) < len(list_found_keywords)) else False
        dict_parse['found_keys'] = ','.join(list(set(list_found_keywords))) # store the list as string. avoid problems later in dataframe
        list_dict.append(dict_parse.copy())
    return list_dict


@checkpoint
def checkpointed_camelot_read(name_file_pdf, i_page):
    return camelot.read_pdf(name_file_pdf, flavor='stream', flag_size=True, pages=str(i_page))
    
    
def wrapper_camelot(name_file_pdf, i_page):
    list_dict = []
    dict_parse = {'page_index': i_page, 'status': 'pass'}
    list_cam = checkpointed_camelot_read(name_file_pdf, i_page)
    # drop cam duplicates checked for data
    list_unique = []
    for i in list_cam:
        if len(list_unique) == 0:
            list_unique.append(i)
        if sum([j.data == i.data for j in list_unique]) == 0:
            list_unique.append(i)
        
    for j, j_cam in enumerate(list_unique):
#        if 0.1 < j_cam.whitespace:
        dict_parse['whitespace'] = j_cam.whitespace
        dict_parse['accuracy'] = j_cam.accuracy
        dict_parse['rows'] = j_cam.df.shape[0]
        dict_parse['cols'] = j_cam.df.shape[1]
        dict_parse['table_name'], dict_parse['table_heading'] = get_cam_table_name(j_cam)
        dict_parse['order_page'] = j
        list_found_keywords = find_keywords_for_relevant_data_columns(j_cam.df)
        dict_parse['repeated_keys'] = True if (len(set(list_found_keywords)) < len(list_found_keywords)) else False
        dict_parse['found_keys'] = ','.join(list(set(list_found_keywords))) # store the list as string. avoid problems later in dataframe
        list_dict.append(dict_parse.copy())
    
    return list_dict



def parse_one_page(wrapper, *args):
    try:
        return wrapper(*args)
    except:
        return [{'page_index': args[1], 'status': 'fail'}]

    

def parse_pdf_for_tables(name_file_pdf, wrapper, parse_pages=None, verbose=False):
    # should find relevant tables on pages: 9, 22, 26, 30, 43, 44, 47
    if parse_pages is None:
        n_pages = get_pdf_n_pages(name_file_pdf)
        parse_pages = list(range(n_pages))
    
    list_dict = []
    for i in parse_pages:
        if verbose: print('parsing:', i)
        list_dict.extend(parse_one_page(wrapper, name_file_pdf, i))
    df = pd.DataFrame(list_dict)
    if df.empty:
        df = pd.DataFrame({'page_index': 0, 'cols': 0, 'rows': 0, 'found_keys': '', 'repeated_keys': False, 'status': 'pass'}, index=[0]) #, 'table_name': '', 'table_heading': ''
    return df



    # camelot visual debugging: camelot.plot(cam, kind='grid') # kind can for flavor='stream' be text / grid / contour / textedge


def parse_old_name_file_wrapper(name_file_pdf):
    str_journal_stem = name_file_pdf.split('/')[-2]    
    journal = str_journal_stem.split('_')[0]
    file_properties = FileProperties()
    file_properties.name_file = name_file_pdf.split('journal_articles/')[-1]
    file_properties.journal = journal
    file_properties.vol = str_journal_stem.split('_')[1]
    file_properties.iss = str_journal_stem.split('_')[2]

    if journal == 'jpe':
        return file_properties
    if journal == 'aer':
        return parse_old_name_file_aer(name_file_pdf, file_properties)
    if journal == 'qje':
        return parse_old_name_file_qje(name_file_pdf, file_properties)  
    # general casee:
    return file_properties        
    

def parse_old_name_file_qje(name_file_pdf, file_properties):
    # parse old style
    # qje_124_1/The Quarterly Journal of Economics-2009-Jayachandran-349-97.pdf
    # ['journal', 'vol', 'iss', 'year', 'author', 'pagefirst', 'pages']
    file_properties.year = name_file_pdf.split('-')[1]
    file_properties.author = name_file_pdf.split('-')[2]
    file_properties.pagefirst = name_file_pdf.split('-')[3]
    #mytabl.pages = name_file_pdf.split('-')[4] # we parse this instead, more reliable
    
    return file_properties


def parse_old_name_file_aer(name_file_pdf, file_properties):
    file_properties.pagefirst = name_file_pdf.strip('.pdf').split('_')[-1]
    return file_properties


def parse_file_name(name_file_pdf):
    pass


@checkpoint
def get_meta_data_about_file(name_file_pdf):
    #check if name_file aligns with new schema
    if FileProperties.check_file_schema_alignment(name_file_pdf):
        #parse the new way
        file_properties = FileProperties()
        file_properties.set_properties_from_file_name(name_file_pdf)
        file_properties.set_name_file()
        return file_properties
        
    else:
        file_properties = parse_old_name_file_wrapper(name_file_pdf)
        file_properties.set_name_file()
    # how many tables does tabula find? pages?
    # how many tables does camelot find? pages?
    # can we get the names of the tables?
    file_properties.pagefirst_parsed = parse_pdf_article_first_page_wrapper(name_file_pdf, file_properties)
    file_properties.pages = str(get_pdf_n_pages(name_file_pdf))
    return file_properties


def join_df_tab_and_cam(df_tabula, df_camelot):
    # assume that if there is only one table on the page, it is correct
    # this will be a damn slow operation but I can't think of any classic simple join logic that works for this
    def _clean_and_join(i_df_tab, i_df_cam):
        if i_df_tab.shape[0] == i_df_cam.shape[0]:
            # let's just hope they are in the right order
            return i_df_tab.reset_index(drop=True).join(i_df_cam.reset_index(drop=True), lsuffix='_tab', rsuffix='_cam')
        else:
            # the sad case when camelot probably picked up some crap. figure out which to remove
            # calculate square of distance between delta cols and deltarows, remove farthest
            list_distances = [((i_row.cols - j_row.cols)**2 + (i_row.rows - j_row.rows)**2, i) for i, i_row in i_df_cam.iterrows() for j, j_row in i_df_tab.iterrows()]
            list_distances.sort(reverse=True)
            worst_index_drop = list_distances[0][1]
            i_df_cam = i_df_cam[i_df_cam.index != worst_index_drop]
            return _clean_and_join(i_df_tab, i_df_cam)
        
    list_rows = []
    for i in df_tabula.page_index.unique():
        i_df_tab = df_tabula[df_tabula.page_index == i]
        i_df_cam = df_camelot[df_camelot.page_index == i]
        list_rows.append(_clean_and_join(i_df_tab, i_df_cam))
    
    df_out = pd.concat(list_rows)
    return df_out


def get_full_table_overview(name_file_pdf):
    df_metadata = pd.DataFrame(get_meta_data_about_file(name_file_pdf).__dict__, index=[0])
    
    df_tabula = parse_pdf_for_tables(name_file_pdf, wrapper_tabula)
    # so far, camelot has not found any table that was interesting that tabula did not so let tabula select the pages to parse
    list_page_index = df_tabula.page_index.tolist()
    df_camelot = parse_pdf_for_tables(name_file_pdf, wrapper_camelot, list_page_index)
    
    #TODO: fix better join logic: how's multi tables with page order matched?
    # join camelot and tabula
    
    
    #df_camel_filter = df_camelot[df_camelot.page_index.isin(df_tabula.page_index)].reset_index(drop=True)
    #df_join = df_tabula.join(df_camel_filter, lsuffix='_tab', rsuffix='_cam')
    df_join = join_df_tab_and_cam(df_tabula, df_camelot)
    df_full_table_overview = pd.concat([df_metadata, df_join], axis=1).fillna(method='ffill')
    
    # ugly fix, there should not be NaNs in these rows, replace with '':
    for i in ['found_keys_tab', 'found_keys_cam']:
        df_full_table_overview[i].fillna('', inplace=True)

    return df_full_table_overview


if __name__ == "__main__":
    # experimentation table
#    i_page = 26
#    list_cam = camelot.read_pdf(file_name_pdf, flavor='stream', flag_size=True, pages=str(i_page))
#    list_tab = tabula.read_pdf(file_name_pdf, silent=True, multiple_tables=True, pages=i_page)
    i_pdf = 'qje_126_4/journal_qje-vol_126-iss_4-year_2011-author_Felkner-pagefirst_2005-pages_57.pdf'
    name_file_pdf = '/home/jonathan/code/anna/dataSource/journal_articles/' + i_pdf

    df_full_table_overview = get_full_table_overview(name_file_pdf)



