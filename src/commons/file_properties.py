#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 15:11:40 2019

@author: jonathan
"""


class FileProperties():
    list_file_name_schema = ['journal', 'vol', 'iss', 'year', 'author', 'pagefirst', 'pages']
    
    def __str__(self):
        list_out = []
        for i in self.__dict__.keys():
            list_out.append(i + ': ' + str(self.__dict__[i]) + '\n')
        return ''.join(list_out)


    def __repr__(self):
        return str(self.__dict__)
    
    
    @classmethod
    def print_schema_properties(cls):
        print(cls.list_file_name_schema)
    
    
    def set_properties(self, journal, vol, iss, year, author, pagefirst, pages):
        # aboslutely need either author or pagefirst for unique id
        self.journal = journal
        self.vol = vol
        self.iss = iss
        self.subfolder = '_'.join([journal, str(vol), str(iss)]).strip('_') 
        self.year = year
        self.author = author
        self.pagefirst = pagefirst
        self.pages = pages


    def set_name_file(self):
        new_name = ''
        for i in self.list_file_name_schema:
            if i in self.__dict__.keys(): new_name += i + '_' + str(self.__dict__[i]) + '-'
        self.name_file =  (new_name.strip('-') + '.pdf').replace(' ', '')
        self.path = '_'.join([self.journal, self.vol, self.iss])
        self.name_file_path = self.path + '/' + self.name_file
        

    
    @classmethod
    def check_file_schema_alignment(cls, file_name_path):
        # check if all keys in filename is in file_name_schema
        file_name = file_name_path.split('/')[-1]
        list_file_properties = file_name.split('-')
        if set([i.split('_')[0] for i in list_file_properties]) == set(cls.list_file_name_schema):
            return True
        else:
            return False
    
   
    def set_properties_from_file_name(self, file_name_path):
        # sanity, check that it aligns, or you create problems
        if self.check_file_schema_alignment(file_name_path):
            file_name = file_name_path.split('/')[-1].strip('.pdf')
            list_file_properties = file_name.split('-')
            for i in list_file_properties:
                (key, val) = i.split('_')
                self.__dict__[key] = val
        else:
            raise('failed schema alignment')

    
    @classmethod
    def check_series_schema_alignment(cls, ser_properties):
        if set(cls.list_file_name_schema).issubset(set(ser_properties.keys())):
            return True
        else:
            return False
        
        
    def set_properties_from_series(self, ser_properties):
        if self.check_series_schema_alignment(ser_properties):
            self.__dict__.update(ser_properties[self.list_file_name_schema].to_dict())
        else:
            raise('failed schema alignment')
            


if __name__ == "__main__":
      
    FileProperties.check_file_schema_alignment('journal_aep-vol_3-iss_4-year_2000-author_xxxx-pagefirst_385-pages_0.pdf')

    file_properties = FileProperties()
    file_properties.set_properties('aep', '3', '4', '2000', 'xxxx', '385', '0')
    file_properties.set_name_file()
    file_properties.check_file_schema_alignment('journal_aep-vol_3-iss_4-year_2000-author_xxxx-pagefirst_385-pages_0.pdf')
#    file_properties.check_file_schema_alignment('journal_aep-vol_3-iss_4-year_2000-author_xxxx-pagefisdfrst_385-pages_0.pdf')
    
    file_properties2 = FileProperties()
#    file_properties2.set_properties_from_file_name('journal_aep-vol_3-iss_4-year_2000-author_xxxx-pagefisdfrst_385-pages_0.pdf')
    file_properties2.set_properties_from_file_name('journal_aep-vol_3-iss_4-year_2000-author_xxxx-pagefirst_385-pages_0.pdf')
    file_properties2

#    file_properties3 = FileProperties()
#    file_properties3.set_properties_from_series(ser_prop)
