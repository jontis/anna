# -*- coding: utf-8 -*-

import os
import pandas as pd
from joblib import dump, load


def save_preprocessed_data_locally(config, forecast_matrix, category, year):
    data_path = get_data_path(config, category, year, should_create_folders=True)
    forecast_matrix.to_csv(data_path, index=False)


def get_data_file_name(config, category, year):
    return '_'.join(['data', config.route, category, config.local_currency, year]) + '.csv'


def get_data_path(config, category, year, should_create_folders=False):
    if should_create_folders and not os.path.exists(config.data_path_gen):
        os.mkdir(config.data_path_gen)
    file_name = get_data_file_name(config, category, year)
    data_path = config.data_path_gen + file_name
    return data_path


def findDataFilesPathLocal(config):
    list_files = os.listdir(config.data_path_gen)
    list_files_out = [config.data_path_gen + i for i in list_files if ('data' in i) & (config.route in i) & (config.local_currency in i) & (config.category in i)]
    return list_files_out


def save_categories_to_forecast_locally(config, list_categories):
    categories_df = pd.DataFrame(list_categories, columns=['categories'])
    categories_df.to_csv(config.data_path_gen + 'categories_' + config.route + '.csv', index=False)


def load_categories_to_forecast_locally(config):
    categories_df = pd.read_csv(config.data_path_gen + 'categories_' + config.route + '.csv')
    return categories_df.categories.tolist()


def load_preprocessed_data_locally(config):
    list_df = []
    for file in findDataFilesPathLocal(config):
        list_df.append(pd.read_csv(file))
    df_out = pd.concat(list_df, sort=True)
    return df_out


def getTuningFilePathLocal(config, category):
    return config.data_path_gen + '_'.join(['tuning_pred_target', config.route, category, config.local_currency]) + '.csv'


def save_predictions_data_locally(config, prediction, category):
    file = getTuningFilePathLocal(config, category)
    df_data = pd.concat([prediction, config.local_currency])
    df_data.to_csv(file)


#def find_valid_model()


def save_model_locally(config, model_package, model_name, latest=False):
    if latest:
        filename = config.data_path_gen + '_'.join(['model_package', config.route, config.category, config.local_currency, model_name, 'latest'])
    else:
        filename = config.data_path_gen + '_'.join(['model_package', config.route, config.category, config.local_currency, str(config.forecasting_date.date()), model_name, ])
    dump(model_package, filename, compress=3)



def load_model_locally(config, model_name, latest=True):
    if latest:
        filename = config.data_path_gen + '_'.join(['model_package', config.route, config.category, config.local_currency, model_name, 'latest'])
    else:
        pass # not implemented yet

    # return empty model package if the file does not exist
    if os.path.isfile(filename):
        model_package = load(filename)
    else:
        print('no file, returned empty model package')
        model_package = {}
    return model_package
