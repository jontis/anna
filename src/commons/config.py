# -*- coding: utf-8 -*-

'''
'''
import os


class Config:

    def __str__(self):
        list_out = []
        for i in self.__dict__.keys():
            list_out.append(i + ': ' + str(self.__dict__[i]) + '\n')
        return ''.join(list_out)


    def __repr__(self):
        return str(self.__dict__)

    data_path_base = __file__.split('src/commons/config.py')[0]
    data_path_gen =  data_path_base + 'dataGenerated/'
    data_path_source = data_path_base + 'dataSource/'
    data_path_articles = data_path_source + 'journal_articles/'
    data_path_labels = data_path_source + 'label_csv/'
