#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 29 19:13:12 2019

@author: jonathan
"""

class MyTable():
    name = ''
    

    def __str__(self):
        list_out = []
        for i in self.__dict__.keys():
            list_out.append(i + ': ' + str(self.__dict__[i]) + '\n')
        return ''.join(list_out)


    def __repr__(self):
        return str(self.__dict__)