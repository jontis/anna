#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 00:40:43 2019

@author: jonathan
"""

#import config
import PyPDF2
#import PyPDF4
import tabula
import camelot
import pandas as pd


def get_cam_table_name(cam):
    # can you find 'table' in any cell in the top 2 rows?
    for i_row in range(2):
        list_table_names = [i.split('table ')[1] for i in cam.df.iloc[i_row, :].apply(lambda x: str(x).lower()) if 'table' in i]
        if 0 < len(list_table_names):
            print(list_table_names[0])
            return list_table_names[0]
    
    print('empty return')
    return ''


def find_keywords_for_relevant_data_columns(df, keywords=['coefficient', 'statistic']):
    # WIP 
    for i_row in range(2):
    list_table_names = [i.split('table ')[1] for i in cam.df.iloc[i_row, :].apply(lambda x: str(x).lower()) if 'table' in i]
    if 0 < len(list_table_names):
        print(list_table_names[0])
    

verbose = False


# modules for 


# pdf file object
# you can find find the pdf file with complete code in below

file_name = '/home/jonathan/code/anna/dataSource/journal_articles/jpe_118_3/653138.pdf'
file_name_test = '/home/jonathan/code/anna/test/foo.pdf'

pdfFileObj = open(file_name, 'rb')

#with open(file_name, 'rb') as pdfFileObj:


# pdf reader object
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

# number of pages in pdf
print(pdfReader.numPages)

# a page object
pageObj = pdfReader.getPage(9)

# extracting text from page.
# this will print the text you can also save that into String
print(pageObj.extractText())


# types of papers: aer, jpe, qje
df_pages = pd.DataFrame(list(range(pdfReader.numPages)), columns=['page_index'])
df_pages['page_number'] = df_results.page_index + 1

# should find relevant tables on pages: 9, 22, 26, 30, 43, 44, 47, 
df_pages['status'] = ''
df_pages['found_table'] = ''
df_pages['rows'] = 0
df_pages['rows'] = 0

list_df = []
for i in list(df_pages.page_index):
    dict_parse = {}
    try:
        df = tabula.read_pdf(file_name, silent=True, pages=i)
        if df is None:
            if verbose: print('read page: ', i, ', successful parse: found no table')
            dict_parse['status'] = 'success'
            dict_parse['found_n_tables'] = 0
            dict_parse['found_n_tables'] = 0
            dict_parse['found_n_tables'] = 0


        else:
            print('read page: ', i, ', successful parse: found table of shape: ', df.shape)    
    except:
        print(i, ', fail')


list_dict = []
for i in list(df_pages.page_index):
    dict_parse = {}
    dict_parse['page_index'] = i
    try:
        list_df = tabula.read_pdf(file_name, silent=True, multiple_tables=True, pages=i)
        dict_parse['status'] = 'pass'
        for j, j_df in enumerate(list_df):
            dict_parse['rows'] = j_df.shape[0]
            dict_parse['cols'] = j_df.shape[1]
            list_dict.append(dict_parse.copy())
    except:
        dict_parse['status'] = 'fail'
        list_dict.append(dict_parse)
df_tabula = pd.DataFrame(list_dict)


list_dict = []
# should find relevant tables on pages: 9, 22, 26, 30, 43, 44, 47, 
for i in list(df_pages.page_index):
    dict_parse = {}
    dict_parse['page_index'] = i
    try:
        #list_cam = camelot.read_pdf(file_name, flavor='stream', pages=str(i))
        list_cam = camelot.read_pdf(file_name, flavor='stream', flag_size=True, pages=str(i))
        dict_parse['status'] = 'pass'
        for j, j_cam in enumerate(list_cam):
            if 0.1 < j_cam.whitespace:
                dict_parse['whitespace'] = j_cam.whitespace
                dict_parse['accuracy'] = j_cam.accuracy
                dict_parse['rows'] = j_cam.df.shape[0]
                dict_parse['cols'] = j_cam.df.shape[1]
                dict_parse['table_name'] = get_cam_table_name(j_cam)
                dict_parse['order_page'] = j
                list_dict.append(dict_parse.copy())
    except:
        dict_parse['status'] = 'fail'
        list_dict.append(dict_parse)
# camelot visual debugging: camelot.plot(cam, kind='grid') # kind can for flavor='stream' be text / grid / contour / textedge
df_camelot = pd.DataFrame(list_dict)
df_camelot['good_parse'] = (df_camelot.accuracy)


# bad parses why?
list_cam = camelot.read_pdf(file_name, flavor='stream', pages=str(47))

# misinterpretations:
# '<s>⫺</s>' = '-'




def table_parser(table):
    pass


    