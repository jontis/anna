#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 29 20:58:04 2019

@author: jonathan
"""

import os
import numpy as np
import pandas as pd
from joblib import Parallel, delayed


#import PyPDF2
from commons.config import Config
from commons.file_properties import FileProperties
from table_extractor import get_full_table_overview
from label_csv_parser import get_labels
from data_set_statistics import print_stats
# find all pdfs

os.environ['CHECKPOINTS_VERBOSITY'] = '0' # set for not having checkpointer spam the console


cfg = Config()


def clean_file_names_in_dir(name_dir):
    for i in os.listdir(cfg.data_path_articles + name_dir):
        new_name = i.replace('%2E', '_')
        os.rename(cfg.data_path_articles + name_dir + '/'  + i, cfg.data_path_articles + name_dir + '/' + new_name)


def build_data_set_from_parsing_pdfs():
    list_dirs = os.listdir(cfg.data_path_articles)
    list_pdf_files = []
    for i_dir in list_dirs:
        clean_file_names_in_dir(i_dir) # file name cleaner
        list_pdf_files.extend([i_dir + '/' + i for i in os.listdir(cfg.data_path_articles + i_dir)])

    list_name_file_path_pdf = [cfg.data_path_articles + i_file for i_file in list_pdf_files]
    list_robust_parallell_wrapper = Parallel(n_jobs=6)(delayed(robust_parallell_table_overview_wrapper)(i_file_path) for i_file_path in list_name_file_path_pdf)

    list_succeded, list_failed, list_df_full_table_overview = [], [], []
    _ = [(list_succeded.append(i[0]), list_failed.append(i[1]), list_df_full_table_overview.append(i[2])) for i in list_robust_parallell_wrapper]
    list_succeded, list_failed, df_full_table_overview = sort_and_compact_list(list_robust_parallell_wrapper)
    df_full_table_overview.fillna('', inplace=True)

    # if you miss pagefirst, can it be filled from pagefirst_parsed?
    df_full_table_overview.loc[df_full_table_overview.pagefirst == '', 'pagefirst'] = df_full_table_overview.loc[df_full_table_overview.pagefirst == '', 'pagefirst_parsed']    
    df_full_table_overview.set_index(['journal', 'vol', 'iss', 'pagefirst'], inplace=True, drop=False)
    df_full_table_overview.sort_index(inplace=True)
    
    return df_full_table_overview


def align_pdf_name_file(ser):
    if ser.name_file_aligned: return False
    if ser.name_file_can_be_aligned:
        if FileProperties.check_series_schema_alignment(ser):
            file_properties = FileProperties()
            file_properties.set_properties_from_series(ser)
            file_properties.set_name_file()
            if os.path.isfile(cfg.data_path_articles + ser.name_file):
                os.rename(cfg.data_path_articles + ser.name_file, cfg.data_path_articles + file_properties.name_file_path)


def name_file_can_be_aligned(ser):
    if len(ser.journal) != 3: return False
    if (ser.vol == '') or (int(ser.vol) < 1): return False
    if (ser.iss == '') or (int(ser.iss) < 1): return False
    # choice, need either author or pagefirst for unique id
    if (ser.pagefirst != '') and (0 < int(ser.pagefirst)): return True
    if (ser.author != '') and (0 < len(ser.author)): return True
    
    # if none of author or pagefirst was present
    return False


def align_all_file_names(df_full_table_overview):
    # align file name to new schema
    df_full_table_overview['name_file_aligned'] = df_full_table_overview.name_file.apply(FileProperties.check_file_schema_alignment)
    df_full_table_overview['name_file_can_be_aligned'] = df_full_table_overview.apply(name_file_can_be_aligned, axis=1)
    df_align_names = df_full_table_overview[(df_full_table_overview.name_file_aligned == False) & (df_full_table_overview.name_file_can_be_aligned == True)]
    for _, i_row in df_align_names.iterrows():
        align_pdf_name_file(i_row)


def robust_parallell_table_overview_wrapper(i_file_path):
    # return schema (file_succeded, file_failed, data)
    try:
        i_df_table_overview = get_full_table_overview(i_file_path)
        return (i_file_path, '', i_df_table_overview)
    except:
        return ('', i_file_path, None)


def sort_and_compact_list(list_robust_parallell_wrapper):
    list_succeded, list_failed, list_df_full_table_overview = [], [], []
    _ = [(list_succeded.append(i[0]), list_failed.append(i[1]), list_df_full_table_overview.append(i[2])) for i in list_robust_parallell_wrapper]
    list_succeded = [i for i in list_succeded if i != '']
    list_failed = [i for i in list_failed if i != '']
    df_full_table_overview = pd.concat(list_df_full_table_overview, sort=False)
    return list_succeded, list_failed, df_full_table_overview


def match_labels(df_full_table_overview, df_labels):
        # match the extracted tables to the manual labels
    # remember, we don't need to match everything to get a dataset
    df_labels['matched'] = False
    df_full_table_overview['matched_table'] = False
    df_full_table_overview['matched_article'] = False
    df_full_table_overview.pagefirst = df_full_table_overview.pagefirst.astype('int64') # convert for comparison
    
    for i, i_row in df_labels.iterrows():
        # start simple matching on table name
        i_bool = ((df_full_table_overview.journal == i_row.journal) & (df_full_table_overview.iss == i_row.iss) & (df_full_table_overview.vol == i_row.vol) & (df_full_table_overview.pagefirst == i_row.pagefirst))
        #i_df_full_table_overview = df_full_table_overview[i_bool]
        df_full_table_overview.loc[i_bool, 'matched_article'] = True
        df_full_table_overview.loc[i_bool & (df_full_table_overview.table_name == i_row.table_name), 'matched_table'] = True
        if i_row.table_name in df_full_table_overview[i_bool].table_name.values.tolist(): 
            df_labels.loc[i, 'matched'] = True
            # need to improve table name detector
        
    return df_full_table_overview, df_labels


def stats_on_found_tables(df_full_table_overview):
    # find the table numbers missing from sequence in the parsed articles
    # test first with tables number 1
    df_articles_found_tables = pd.DataFrame(index=df_full_table_overview.index.unique())
    
    def get_table_names(index_selector):
        table_names = df_full_table_overview.loc[index_selector, 'table_name'].values
        return table_names
    
        
        
    df_articles_found_tables['table_names'] = df_articles_found_tables.index.map(get_table_names) # map because apply cant be used on index
    df_articles_found_tables['n_tables'] = df_articles_found_tables.table_names.apply(len)
    df_articles_found_tables['table_1_found'] = df_articles_found_tables.table_names.apply(lambda x: '1' in x)
    df_articles_found_tables['table_sorted_sequence'] = df_articles_found_tables.table_names.apply(lambda x: sorted([int(i) for i in x if i.isnumeric()]))
    df_articles_found_tables['table_numbers_found'] = df_articles_found_tables.table_sorted_sequence.apply(lambda x: 0 < len(x))
    df_articles_found_tables['table_n_max'] = df_articles_found_tables.table_sorted_sequence.apply(lambda x: x[-1] if (0 < len(x)) else 0)
    df_articles_found_tables['table_i_name_duplicates'] = df_articles_found_tables.table_sorted_sequence.apply(lambda x: [i for (i, j) in zip(x[:-1], x[1:]) if ((1 < len(x)) & (i == j))])
    df_articles_found_tables['tables_missing_from_sequence'] = df_articles_found_tables.apply(lambda x: [i for i in list(range(1, x.table_n_max + 1)) if i not in x.table_sorted_sequence], axis=1)
    
    return df_articles_found_tables


def build_balanced_data_set(df_full_table_overview):
    #TODO: stratify sampling later?
    # create data set for training from df_full_table_overview that was extracted by just analysing the pdfs without knowing the labels
    df_full_table_overview.set_index(['journal', 'vol', 'iss', 'pagefirst'], inplace=True, drop=True)
    df_matched_articles_with_labeled_matched_table = df_full_table_overview.loc[df_full_table_overview.index.unique()]
    df_data_true = df_matched_articles_with_labeled_matched_table[df_matched_articles_with_labeled_matched_table.matched_table == 1]
    # select the same amount of tables from from articles that we're not interesting
    # since matching was done on table_name, this can't be '' in the false set as this gives a very skewed data set and a bad feature
    df_data_false = df_matched_articles_with_labeled_matched_table[(df_matched_articles_with_labeled_matched_table.matched_table == 0) & (df_matched_articles_with_labeled_matched_table.table_name != '')]
    df_data_false = df_data_false.sample(n=df_data_true.shape[0])
    df_data = pd.concat([df_data_true, df_data_false])
    
    return df_data


def generate_data_set():
    df_full_table_overview = build_data_set_from_parsing_pdfs()
        # pull in the excel files with labels
    list_label_csv_files = [i for i in os.listdir(cfg.data_path_labels) if i[-4:] == '.csv']
    list_df_labels = [get_labels(cfg.data_path_labels + i, verbose=False) for i in list_label_csv_files]
    df_labels = pd.concat(list_df_labels, sort=False).reset_index(drop=True)

    df_full_table_overview, df_labels = match_labels(df_full_table_overview, df_labels)

    df_articles_found_tables = stats_on_found_tables(df_full_table_overview)
    
    df_data = build_balanced_data_set(df_full_table_overview)

    return df_full_table_overview, df_labels, df_articles_found_tables, df_data


if __name__ == "__main__":    
    df_full_table_overview, df_labels, df_articles_found_tables, df_data = generate_data_set()
    #align_all_file_names(df_full_table_overview)

    print_stats(df_full_table_overview, df_labels, df_articles_found_tables)
    
    #df_labels_return = get_labels(name_file_csv, verbose=True)
    
    # persist tables as cache to be able to run on small machines
    df_full_table_overview.to_csv(cfg.data_path_gen + 'df_full_table_overview.csv')
    df_labels.to_csv(cfg.data_path_gen + 'df_labels.csv')
    df_articles_found_tables.to_csv(cfg.data_path_gen + 'df_articles_found_tables.csv')
    df_data.to_csv(cfg.data_path_gen + 'df_data.csv')
