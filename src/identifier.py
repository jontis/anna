# -*- coding: utf-8 -*-

import os
import numpy as np
import pandas as pd
import sklearn.model_selection
import sklearn.ensemble
import sklearn.preprocessing
import sklearn.metrics
import sklearn.feature_selection
import matplotlib.pyplot as plt

from commons.config import Config
from data_set_generator import generate_data_set
from data_set_statistics import print_stats

cfg = Config()


# check for cached copies instead of generating:
if os.path.exists(cfg.data_path_gen + 'df_full_table_overview.csv'):
    df_full_table_overview = pd.read_csv(cfg.data_path_gen + 'df_full_table_overview.csv')
    df_labels = pd.read_csv(cfg.data_path_gen + 'df_labels.csv')
    df_articles_found_tables = pd.read_csv(cfg.data_path_gen + 'df_articles_found_tables.csv')
    df_data = pd.read_csv(cfg.data_path_gen + 'df_data.csv')
else:
    df_full_table_overview, df_labels, df_articles_found_tables, df_data = generate_data_set()
print_stats(df_full_table_overview, df_labels, df_articles_found_tables)

# ugly fix, there should not be NaNs in these rows, replace with '':
for i in ['found_keys_tab', 'found_keys_cam']:
    df_full_table_overview[i].fillna('', inplace=True)
    df_data[i].fillna('', inplace=True)

df_data.reset_index(inplace=True)
y = df_data.matched_table
X = df_data.drop(['matched_table'], axis=1)


# convert all feature types to model understandable
def feature_select_and_transform(X):
    def labelize(ser):
        if 50 < ser.nunique(): raise('to many categories') 
        le = sklearn.preprocessing.LabelEncoder()
        ser = le.fit_transform(ser)
        return ser, le
        
    list_features_categorical = ['table_name'] # 'journal', 'vol', 'iss', 'year', 
    list_features_numeric = ['pages', 'page_index_tab', 'rows_tab', 'cols_tab', 'page_index_cam', 'whitespace', 'accuracy', 'rows_cam', 'cols_cam', 'order_page']
    list_features_bool = ['repeated_keys_tab', 'repeated_keys_cam']
    one_hot_feats = ['found_keys_tab', 'found_keys_cam']
    # generate features from table_heading
    
    # labelize the categories
    list_X_cat = []
    dict_le = {}
    for i in list_features_categorical:
        #print(i)
        arr, le = labelize(X[i])
        ser = pd.Series(arr, name=i)
        list_X_cat.append(ser)
        dict_le.update({i: le})    
    X_cat = pd.concat(list_X_cat, axis=1)
    
    # make proper numericals
    list_X_num = []
    for i in list_features_numeric:
        #print(i)
        list_X_num.append(X[i].apply(lambda x: x if x != '' else -1).astype('float'))
    X_num = pd.concat(list_X_num, axis=1)

    # bools
    X_bool = X[list_features_bool]
    
    # found keys
    list_X_keys = []
    list_keys = ['statistic', 'variable', 'coefficient']
    for i in list_keys:
        ser = X['found_keys_tab'].apply(lambda x: 1 if i in x.split(',') else 0)
        ser.name = 'key_tab_' + i
        list_X_keys.append(ser)
        ser = X['found_keys_cam'].apply(lambda x: 1 if i in x.split(',') else 0)
        ser.name = 'key_cam_' + i
        list_X_keys.append(ser)
    X_keys = pd.concat(list_X_keys, axis=1)
    
    X_keys['key_any'] = X_keys.any(axis=1)
        
    return pd.concat([X_cat, X_num, X_bool, X_keys], axis=1), dict_le


X, dict_le = feature_select_and_transform(X)

splitter = sklearn.model_selection.StratifiedShuffleSplit(2, test_size=.3)

train_index, test_index = next(splitter.split(X, y))
X_train, y_train = X.iloc[train_index], y.iloc[train_index]
X_test, y_test = X.iloc[test_index], y.iloc[test_index]
# try training simple extra forest classifier

model = sklearn.ensemble.ExtraTreesClassifier(n_estimators=100, bootstrap=True, oob_score=True)

model.fit(X_train, y_train)

df_feat = pd.DataFrame(model.feature_importances_, index=X.columns, columns=['importance'])

df_feat.sort_values('importance', ascending=False)


y_test.name = 'y_test'
df_test = pd.DataFrame(y_test)
df_test['pred'] = model.predict(X_test)
df_test['correct'] = (df_test.y_test == df_test.pred)
df_test['proba'] = model.predict_proba(X_test)[:, 1] # probabilities for class 1


#model.score(X_test, y_test)

cv = sklearn.model_selection.StratifiedKFold(n_splits=10, random_state=42, shuffle=True)
score = sklearn.model_selection.cross_val_score(model, X, y, cv=cv).mean().round(2)

print('A vanilla Extra Trees Classification model scores ' + str(score) + ' on a 10 fold cross validation, when trying to identify if a table would be selected by a human or not.')
print('The distribution of false positives and negatives seems to be balanced')


# roc curve
fpr, tpr, _ = sklearn.metrics.roc_curve(df_test.y_test, df_test.proba)
plt.plot(fpr, tpr, label='ROC')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
print('AUC for this ROC is ' + str(sklearn.metrics.roc_auc_score(df_test.y_test, df_test.proba).round(3)))


# try to push it a little by RFE
estimator = sklearn.ensemble.ExtraTreesClassifier(n_estimators=100, bootstrap=True, oob_score=True)
selector = sklearn.feature_selection.RFECV(estimator, step=1, cv=cv, verbose=True)
selector = selector.fit(X, y)
estimator.fit(X_train.loc[:, selector.support_], y_train)

# which features have support?
X.columns[selector.support_]
score = sklearn.model_selection.cross_val_score(estimator, X.loc[:, selector.support_], y, cv=cv).mean().round(2)

# new ROC Curve
# roc curve
df_test['proba2'] = estimator.predict_proba(X_test.loc[:, selector.support_])[:, 1] # probabilities for class 1
fpr, tpr, _ = sklearn.metrics.roc_curve(df_test.y_test, df_test.proba2)
plt.plot(fpr, tpr, label='ROC')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
print('AUC for this ROC is ' + str(sklearn.metrics.roc_auc_score(df_test.y_test, df_test.proba2).round(3)))

