#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 16:52:45 2019

component: parse the source csv as labels

@author: jonathan
"""


from commons.config import Config
from commons.my_table import MyTable
import pandas as pd
import matplotlib.pyplot as plt



def get_labels(name_file_csv):
    df_labels = pd.read_csv(name_file_csv, sep=';')
    df_labels.dropna(axis=0, how='all', thresh=10, inplace=True) # drop empty rows from excel
    
    list_df_tables_short = []
    for i_pagefirst in df_labels.article_page.unique():
        
        df_labels_article = df_labels[df_labels.article_page == i_pagefirst]
        list_table_names = list(df_labels_article.table_panel.unique())
    
        list_labeled_tables = []
        for i in list_table_names:
            mytabl = MyTable()
            df_table = df_labels_article[df_labels_article.table_panel == i]
            str_journal_stem = name_file_csv.split('/')[-1].split('.')[0]
            
            mytabl.journal = str_journal_stem.split('_')[0]
            mytabl.vol = str_journal_stem.split('_')[1]
            mytabl.iss = str_journal_stem.split('_')[2]
            mytabl.pagefirst = i_pagefirst
            
            # collect extra info from label table that can help matching or feature engineering
            mytabl.model= df_table.model.values[0]
            mytabl.type = df_table.type.values[0]
            mytabl.type_emp = df_table.type_emp.values[0]
            mytabl.stars = df_table.stars.values[0]
            mytabl.main= df_table.main.values[0]
            mytabl.ra = df_table.ra.values[0]
        
            mytabl.table_name = i
            mytabl.n_values = df_table.shape[0]
            mytabl.n_rows = df_table.row.nunique()
            mytabl.n_cols = df_table.column.nunique()
            list_value_features = list(df_table.iloc[:, df_table.columns.isin(['coefficient', 't_stat', 'p_value', 'standard_deviation', 'standard_deviation_2', 'standard_deviation_3'])].dropna(axis=1).columns)
            mytabl.list_value_features = ','.join(list(set(list_value_features))) # store the list as string. avoid problems later in dataframe
            list_labeled_tables.append(mytabl)
        
        
        df_tables_short = pd.DataFrame([i.__dict__ for i in list_labeled_tables])
        list_df_tables_short.append(df_tables_short)
        
    df_labels_return = pd.concat(list_df_tables_short)
    return df_labels_return


if __name__ == "__main__":
    # experimentation table
#    i_page = 26
#    list_cam = camelot.read_pdf(file_name_pdf, flavor='stream', flag_size=True, pages=str(i_page))
#    list_tab = tabula.read_pdf(file_name_pdf, silent=True, multiple_tables=True, pages=i_page)
    name_file_csv = '/home/jonathan/code/anna/dataSource/label_csv/jpe_118_3.csv'

    df_tables_short = get_labels(name_file_csv)
