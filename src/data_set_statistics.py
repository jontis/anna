#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 12:11:20 2019

@author: jonathan
"""


def print_stats(df_full_table_overview, df_labels, df_articles_found_tables):
    # stats on the dataset
    # what ratio of articles have at least one table matched
    n_labeled_articles = df_labels.loc[:, ['journal', 'vol', 'iss', 'pagefirst']].drop_duplicates().shape[0]
    n_labeled_articles_matched = df_labels.loc[(df_labels.matched == True), ['journal', 'vol', 'iss', 'pagefirst']].drop_duplicates().shape[0]
    print('Ratio of labeled articles with at least one table matched in training set:', round(n_labeled_articles_matched / n_labeled_articles, 3))
    
    n_labeled_tables = df_labels.shape[0]
    n_labeled_tables_matched = df_labels.matched.sum()
    print('Ratio of labeled tables matched in training set:', round(n_labeled_tables_matched / n_labeled_tables, 3))
    
    # how many articles did we get a table from? (not really relevant because we get extra tables that are not really there)
    #n_parsed_articles = len(list_pdf_files)
    #n_parsed_articles_with_table_extracted = df_full_table_overview.loc[:, ['journal', 'vol', 'iss', 'pagefirst']].drop_duplicates().shape[0]
    
    # of the articles where we succesfully matched at least one table, how big ratio of labeled tables was matched?
    # create multiindex for selecting records
    df_match_articles = df_labels.loc[(df_labels.matched == True), ['journal', 'vol', 'iss', 'pagefirst']].drop_duplicates()
    df_multi = df_labels.set_index(['journal', 'vol', 'iss', 'pagefirst'])
    df_labels_match_articles = df_multi.loc[df_match_articles.set_index(['journal', 'vol', 'iss', 'pagefirst']).index]
    print('Ratio of labeled tables matched in the articles that has at least one match:', df_labels_match_articles.matched.mean().round(3))    
    
    print('Ratio of parsed table headings for the matched articles:', round(sum((df_full_table_overview.matched_table == 1) & (df_full_table_overview.table_heading != '')) / sum(df_full_table_overview.matched_table == 1), 3))
    
    print('Ratio of parsed table numbers where 1 was found:', df_articles_found_tables.table_1_found.mean().round(3))
