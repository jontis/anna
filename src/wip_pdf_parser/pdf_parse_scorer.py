#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 10:50:38 2019

goal with this component:
    score the success of the parsed table

features:
    number of rows / columns, reasonable or not
    table heading quality
    column names quality
    figures quality
    amount of non alphanum characters
    

@author: jonathan
"""

import os
import random
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from checkpointer import checkpoint
import camelot


#import PyPDF2
from commons.config import Config
from commons.file_properties import FileProperties

from table_extractor import parse_pdf_for_tables, wrapper_tabula
os.environ['CHECKPOINTS_VERBOSITY'] = '0' # set for not having checkpointer spam the console

# find all pdfs
# read the available libraries of pdfs


cfg = Config()

list_dirs = os.listdir(cfg.data_path_articles)
list_pdf_files = []
for i_dir in list_dirs:
    list_pdf_files.extend([i_dir + '/' + i for i in os.listdir(cfg.data_path_articles + i_dir)])
list_name_file_path_pdf = [cfg.data_path_articles + i_file for i_file in list_pdf_files]


def get_random_pdfs(list_pdf_files, k=10):
    random.seed(42) # set the random state to make sure we always get the same list, avoid random in the statistics
    output_pdf_list = random.sample(list_name_file_path_pdf, k)
    return output_pdf_list


# randomize a selection of tiles to test, static random, smart up later
list_pdfs = get_random_pdfs(list_pdf_files, k=100)

# use tabula to find the relevant table pages in the pdfs, tabula has been better at this
def get_pages(name_file_pdf):
    df_tabula = parse_pdf_for_tables(name_file_pdf, wrapper_tabula)
    df_article_pages = df_tabula[['page_index', 'status', 'rows', 'cols']].copy()
    df_article_pages['name_file_pdf'] = name_file_pdf
    return df_article_pages


def robust_parallell_table_page_wrapper(name_file_pdf):
    # return schema (file_succeded, file_failed, data)
    try:
        df_article_pages = get_pages(name_file_pdf)
        return df_article_pages # this can return status: fail, page_index: NaN
    except:
        print(name_file_pdf)
        return pd.DataFrame({'page_index': 0, 'status': 'fail', 'name_file_pdf': name_file_pdf}, index=[1])

#str_example_pdf = '/home/jonathan/code/anna/dataSource/journal_articles/aer_100_1/journal_aer-vol_100-iss_1-year_-author_-pagefirst_70-pages_30.pdf' # tabula misses multiple tables on p22

# parse the selection


# start with a tabula parse as is was good in finding the pages with tables
list_robust_parallell_wrapper = Parallel(n_jobs=6)(delayed(robust_parallell_table_page_wrapper)(i_file_path) for i_file_path in list_pdfs)

df_article_table_pages = pd.concat(list_robust_parallell_wrapper)


df_strange = df_article_table_pages[df_article_table_pages.name_file_pdf.isin(df_article_table_pages[df_article_table_pages.status == 'fail'].name_file_pdf)]


# only keep the 'pass' for now
df_pass = df_article_table_pages.loc[df_article_table_pages.status == 'pass', ['name_file_pdf', 'page_index', 'rows', 'cols']]
df_pass = df_pass[(2<df_pass.rows) & (2<df_pass.cols)] # just select large enough. almost perfect at this point, missed some in one pdf


# protptype checkpointed camelot read that takes parsing options but has defaults
@checkpoint
def checkpointed_camelot_read(name_file_pdf, i_page, **kwargs):
    if kwargs == {}:
        return camelot.read_pdf(name_file_pdf, pages=str(i_page), flavor='stream', flag_size=True)
    else:
        return camelot.read_pdf(name_file_pdf, pages=str(i_page), **kwargs)


# helpers for quality functions
def ratio_of_empty_cells(df_in):
    ratio = df_in.applymap(lambda x: x == '').sum().sum() / df_in.count().sum()
    return ratio


def str_data(cam_in):
    str_temp = [i for j in cam_in.data for i in j if i != '']
    return ''.join(str_temp)
    

def str_clean(str_in, list_clean=['<s>', '</s>']):
    str_temp = str_in
    for i in list_clean:
        str_temp = str_temp.replace(i, '')
    return str_temp


def n_non_alnum(str_in):
    return sum([i.isalnum() for i in str_in])


def test(**kwargs):
    print(**kwargs)

    
# do a camelot parse for the relevant pages
# sum the whole pdf or just table for table?
# suspect that the whole pdf can use the same parse options
def evaluate_parse_quality(name_file_pdf):
    # list
    # of
    # qulity 
    # functions
    # here
    
    # parse  pdf
    pdf0 = '/home/jonathan/code/anna/dataSource/journal_articles/aer_100_1/journal_aer-vol_100-iss_1-year_-author_-pagefirst_70-pages_30.pdf'
    j_cam = checkpointed_camelot_read(pdf0, 12)[0] # assume there was just one table for now, improve later
    
    # ratio of empty cells. high is probaly bad
    ratio_of_empty_cells(j_cam.df)
    
    
    # string data
    str_data0 = str_clean(str_data(j_cam))

    # strange signs
    
    
    # superfluous '<s>s</s>'
    # strange empty cells
    # almost empty rows or cols
    # n of pure numbers in df
    # 
    
    return df_with_quality_measures


#TODO: use camelot to parse qulity related features
    

list_parse_quality_features = Parallel(n_jobs=6)(delayed(_)(i_file_path) for i_file_path in list_pdfs)





# automatically evaluate the parse

# evaluate and compare manually

# store the results
# df cols: filename, page_index, page_order, features




# debug
pdf2 = '/home/jonathan/code/anna/dataSource/journal_articles/qje_124_2/journal_qje-vol_124-iss_2-year_2009-author_Manuelli-pagefirst_771-pages_37.pdf'
df_tabula = parse_pdf_for_tables(pdf2, wrapper_tabula)
# should get tables at 19, 21, 25, 30, not any others
df_tabula.dropna(inplace=True)
df_tabula = df_tabula[(2<df_tabula.cols) & (2<df_tabula.rows)] # just select large enough. almost perfect at this point, missed some in one pdf


pdf0 = '/home/jonathan/code/anna/dataSource/journal_articles/aer_100_1/journal_aer-vol_100-iss_1-year_-author_-pagefirst_70-pages_30.pdf'
j_cam = checkpointed_camelot_read(pdf0, 12)
str_data0 = str_clean(str_data(j_cam))
